<?php

namespace MiniLive;

trait common
{
    protected $error = null;
    protected $error_list = [
        '300001'  => '禁止创建/更新商品 或 禁止编辑&更新房间',
        '300002'  => '名称长度不符合规则(长度必须大于2个汉字)',
        '300006'  => '图片上传失败（如：mediaID过期）',
        '300010'  => '商品审核状态不对（如：商品审核中）',
        '300015'  => '商品ID不存在',
        '300017'  => '商品未提审',
        '300018'  => '产品图片宽度或高度超过300px',
        '300022'  => '此房间号不存在',
        '300023'  => '房间状态异常（当前房间状态不允许此操作）',
        '300024'  => '商品不存在',
        '300025'  => '商品审核未通过',
        '300026'  => '房间商品数量已经满额',
        '300027'  => '导入商品失败',
        '300028'  => '房间名称违规',
        '300029'  => '主播昵称违规',
        '300031'  => '直播间封面图不合规',
        '300032'  => '直播间分享图违规',
        '300033'  => '添加商品超过直播间上限',
        '300034'  => '主播微信昵称长度不符合要求',
        '300035'  => '主播微信号不存在',
        '300036'  => '主播需先完成实名验证才能开播',
        '300037'  => '购物直播频道封面图不合规',
        '300038'  => '未在小程序管理后台配置客服',
        '300039'  => '主播副号微信号不合法',
        '300040'  => '名称含有非限定字符（含有特殊字符）',
        '300041'  => '创建者微信号不合法',
        '300043'  => '每天只允许一场直播开启关注',
        '300045'  => '讲解视频未生成',
        '300046'  => '讲解视频生成失败',
        '300047'  => '已有商品正在推送，请稍后再试',
        '300048'  => '拉取商品列表失败',
        '300049'  => '商品推送过程中不允许上下架',
        '300050'  => '排序商品列表为空',
        '300051'  => '解析JSON出错',
        '300052'  => '已下架的商品无法推送',
        '300053'  => '直播间未添加此商品',
        '400001'  => '微信号不合规或不存在',
        '400002'  => '微信号需要实名认证，仅设置主播角色时可能出现',
        '400003'  => '添加角色达到上限（管理员10个，运营者500个，主播500个）',
        '400004'  => '重复添加角色',
        '400005'  => '主播角色删除失败，该主播存在未开播的直播间',
        '500001'  => '副号不合规',
        '500002'  => '副号未实名',
        '500003'  => '已经设置过副号了，不能重复设置',
        '500004'  => '不能设置重复的副号',
        '500005'  => '副号不能和主号重复',
        '600001'  => '用户已被添加为小助手',
        '600002'  => '找不到用户',
        '9410000' => '直播间列表为空',
        '9410001' => '获取房间失败',
        '9410002' => '获取商品失败',
        '9410003' => '获取回放失败',
    ];
    
    /**
     * 处理返回(适用于返回true 或 false)
     *
     * @param array  $res    返回数据
     * @param string $field1 当res包含指定字段时，返回整个res
     * @param string $field2 当res包含指定字段时，返回整个res
     */
    protected function handleReturn($res, $field1 = '', $field2 = '')
    {
        if ($field1 && isset($res[$field1]) && $res[$field1]) {
            return $res;
        } elseif ($field2 && isset($res[$field2]) && $res[$field2]) {
            return $res;
        } elseif (isset($res['errcode']) && $res['errcode'] == 0) {
            return $res;
        } else {
            if (isset($res['errmsg']) && !$res['msg']) {
                $res['msg'] = $res['errmsg'] . (isset($res['errcode']) ? '(' . $res['errcode'] . ')' : '');
            }
            $this->setError($res);
            return false;
        }
    }
    
    /**
     * 本地远程图片下载到本地，并上传到微信服务器，返回media_id
     *
     * @param string $file_path          本地图片路径 或 远程图片地址(http开头)
     * @param int    $limit_image_width  限制图片宽度，如：300，0表示不限制
     * @param int    $limit_image_height 限制图片高度，如：300，0表示不限制
     * @return false|mixed|string
     */
    protected function imageFileToMediaId($file_path, $limit_image_width = 0, $limit_image_height = 0)
    {
        if (!$file_path) {
            return '';
        }
        // 如果是远程图片，先下载到本地
        if (strpos($file_path, 'http') !== false) {
            $file_path = $this->downTempImage($file_path);
        }
        
        // 如果是本地图片，直接上传
        if (file_exists($file_path)) {
            // 限制图片尺寸
            if ($limit_image_width || $limit_image_height) {
                $image_info = getimagesize($file_path);
                if ($limit_image_width && $image_info[0] > $limit_image_width) {
                    $this->setError(['msg' => "封面宽度不能超过{$limit_image_width}px"]);
                    return false;
                } else if ($limit_image_height && $image_info[1] > $limit_image_height) {
                    $this->setError(['msg' => "封面高度不能超过{$limit_image_width}px"]);
                    return false;
                }
            }
            
            if ($media_info = $this->mediaUpload($file_path)) {
                $file_path = $media_info['media_id'];
            } else {
                return false;
            }
        }
        return $file_path;
    }
    
    /**
     * 下载远程图片到本地(临时目录)
     *
     * @param string $url 远程图片地址
     * @return false|string
     */
    protected function downTempImage($url)
    {
        $res = Http::httpGet($url);
        if (!$res) {
            return false;
        }
        $file_path = tempnam(sys_get_temp_dir(), 'mini_live_');
        file_put_contents($file_path . '.jpg', $res);
        
        // 进程结束时删除临时文件
        register_shutdown_function(function () use ($file_path) {
            if (file_exists($file_path . '.jpg')) {
                unlink($file_path);
                unlink($file_path . '.jpg');
            }
        });
        return $file_path . '.jpg';
    }
    
    public function getError()
    {
        return $this->error;
    }
    
    private function setError($error)
    {
        if (isset($this->error_list[$error['errcode']])) {
            $error['msg'] = $this->error_list[$error['errcode']];
        }
        $this->error = $error;
        return false;
    }
}
