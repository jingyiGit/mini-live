<?php

namespace MiniLive;

/**
 * 成员管理
 */
trait role
{
    /**
     * 设置成员角色
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/role-manage.html#1
     *
     * @param string $username 用户的微信号
     * @param int    $role     角色，0-超级管理员，1-管理员，2-主播，3-运营者
     * @return array|false
     */
    public function roleSet($username, $role)
    {
        $param = [
            'username' => $username,
            'role'     => $role,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/role/addrole?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 解除成员角色
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/role-manage.html#2
     *
     * @remark 移除小程序直播成员的管理员、运营者和主播角色
     * @param string $username 用户的微信号
     * @param int    $role     取值[1-管理员，2-主播，3-运营者]，删除超级管理员将无效
     * @return array|false
     */
    public function roleDelete($username, $role)
    {
        $param = [
            'username' => $username,
            'role'     => $role,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/role/deleterole?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 查询成员列表
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/role-manage.html#3
     *
     * @remark 移除小程序直播成员的管理员、运营者和主播角色
     * @param int    $role    查询的用户角色，取值 [-1-所有成员， 0-超级管理员，1-管理员，2-主播，3-运营者]，默认-1
     * @param string $keyword 搜索的微信号或昵称，不传则返回全部
     * @param int    $page    分页查询的页数，默认1
     * @param int    $limit   分页查询的每页大小，默认10，不超过30
     * @return array|false
     */
    public function roleList($role = -1, $keyword = '', $page = 1, $limit = 10)
    {
        if (!in_array($role, [-1, 0, 1, 2, 3])) {
            return $this->setError(['msg' => 'role参数错误']);
        }
        $param = [
            'role'         => $role,
            'keyword'      => $keyword,
            'offset'       => ($page - 1) * $limit,
            'limit'        => $limit,
            'access_token' => $this->access_token,
        ];
        $res   = Http::httpGet($this->api_url . '/wxaapi/broadcast/role/getrolelist', $param);
        return $this->handleReturn($res);
    }
}
