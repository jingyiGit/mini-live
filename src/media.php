<?php

namespace MiniLive;

trait media
{
    /**
     * 临时素材上传
     * 小程序目前不支持支持永久素材
     * https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/New_temporary_materials.html
     *
     * @param string $filePath 文件路径
     * @return array|bool
     */
    public function mediaUpload($filePath)
    {
        if (!file_exists($filePath)) {
            $this->setError('要上传的文件不存在，请检查路径是否有误: ' . $filePath);
            return false;
        }
        
        $param = [
            'type'         => 'image',
            'access_token' => $this->getAccessToken(),
        ];
        
        $res = Http::httpUpload($this->api_url . "/cgi-bin/media/upload", ['media' => $filePath], $param);
        return $this->handleReturn($res, 'media_id');
    }
    
    /**
     * 获取临时素材
     *
     * @param string $media_id 媒体ID
     * @return array|bool
     */
    public function mediaGet($media_id)
    {
        $url = $this->api_url . "/cgi-bin/media/get?access_token=" . $this->getAccessToken() . '&media_id=' . $media_id;
        return Http::httpGet($url);
    }
}
