<?php

namespace MiniLive;

class MiniLive
{
    use common;
    use media;
    use broadcast;
    use good;
    use role;
    
    protected $api_url = 'https://api.weixin.qq.com';
    protected string $access_token = '';
    
    public function __construct($access_token)
    {
        $this->access_token = $access_token;
    }
    
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }
    
    public function getAccessToken()
    {
        return $this->access_token;
    }
}
