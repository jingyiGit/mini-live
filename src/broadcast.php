<?php

namespace MiniLive;

/**
 * 直播间接口
 */
trait broadcast
{
    /**
     * 创建直播间 / 编辑直播间
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_1-%E5%88%9B%E5%BB%BA%E7%9B%B4%E6%92%AD%E9%97%B4
     *
     * @remark 有提供id(直播间id)则为修改
     * @return array|bool
     */
    public function roomCreate($param)
    {
        // 直播计划开始时间（开播时间需要在当前时间的10分钟后 并且 开始时间不能在 6 个月后）
        if (!isset($param['startTime'])) {
            $param['startTime'] = time() + 610;
        } else if (strpos($param['startTime'], ':')) {
            $param['startTime'] = strtotime($param['startTime']);
        }
        // 开播时间需要在当前时间的10分钟后 并且 开始时间不能在 6 个月后
        if ($param['startTime'] > time() + 86400 * 179) {
            $param['startTime'] = time() + 86400 * 179;
        } else if ($param['startTime'] < (time() + 610)) {
            $param['startTime'] = time() + 610;
        }
        
        // 直播计划结束时间（开播时间和结束时间间隔不得短于30分钟，不得超过24小时）
        if (!isset($param['endTime'])) {
            $param['endTime'] = time() + 86400;
        } else if (strpos($param['endTime'], ':')) {
            $param['endTime'] = strtotime($param['endTime']);
        }
        if ($param['endTime'] - $param['startTime'] < 1800) {
            $param['endTime'] = $param['startTime'] + 1810;
        } else if ($param['endTime'] - $param['startTime'] > 86400) {
            $param['endTime'] = $param['startTime'] + 86400;
        }
        
        // 直播间背景图，图片规则：建议像素1080*1920，大小不超过2M
        $param['coverImg'] = $this->imageFileToMediaId($param['coverImg']);
        
        // 直播间分享图，图片规则：建议像素800*640，大小不超过1M；
        $param['shareImg'] = $this->imageFileToMediaId($param['shareImg']);
        
        // 购物直播频道封面图，图片规则：建议像素800*800，大小不超过100KB；
        $param['feedsImg'] = $this->imageFileToMediaId($param['feedsImg']);
        
        // 直播类型，1：推流，0：手机直播
        if (!isset($param['type'])) {
            $param['type'] = 0;
        }
        
        // 是否关闭点赞 【0：开启，1：关闭】（若关闭，观众端将隐藏点赞按钮，直播开始后不允许开启）
        if (!isset($param['closeLike'])) {
            $param['closeLike'] = 0;
        }
        
        // 是否关闭货架 【0：开启，1：关闭】（若关闭，观众端将隐藏商品货架，直播开始后不允许开启）
        if (!isset($param['closeGoods'])) {
            $param['closeGoods'] = 0;
        }
        
        // 是否关闭评论 【0：开启，1：关闭】（若关闭，观众端将隐藏评论入口，直播开始后不允许开启）
        if (!isset($param['closeComment'])) {
            $param['closeComment'] = 0;
        }
        $path = isset($param['id']) ? '/wxaapi/broadcast/room/editroom' : '/wxaapi/broadcast/room/create';
        $res  = Http::httpPostJson($this->api_url . $path . '?access_token=' . $this->access_token, $param);
        if (isset($res['errmsg'])) {
            if (strpos($res['errmsg'], 'parameter shareImg must') !== false) {
                $res['msg'] = '请上传 分享卡片封面';
            } else if (strpos($res['errmsg'], 'parameter feedsImg must') !== false) {
                $res['msg'] = '请上传 官方收录封面';
            } else if (strpos($res['errmsg'], 'parameter coverImg must') !== false) {
                $res['msg'] = '请上传 直播间背景墙';
            }
        }
        if ($res = $this->handleReturn($res)) {
            $res['coverImg']  = $param['coverImg'];
            $res['shareImg']  = $param['shareImg'];
            $res['feedsImg']  = $param['feedsImg'];
            $res['startTime'] = $param['startTime'];
            $res['endTime']   = $param['endTime'];
            return $res;
        } else {
            return false;
        }
    }
    
    /**
     * 获取直播间列表
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_2-%E8%8E%B7%E5%8F%96%E7%9B%B4%E6%92%AD%E9%97%B4%E5%88%97%E8%A1%A8
     *
     * @param int $page  页码
     * @param int $limit 每页数量
     * @return array|false
     */
    public function roomGetliveinfo($page = 1, $limit = 10)
    {
        $param = [
            'start' => ($page - 1) * $limit,
            'limit' => $limit,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxa/business/getliveinfo?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 获取直播间回放
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_2-%E8%8E%B7%E5%8F%96%E7%9B%B4%E6%92%AD%E9%97%B4%E5%88%97%E8%A1%A8
     *
     * @param int $room_id 直播间id
     * @param int $page    页码
     * @param int $limit   每页数量
     * @return array|false
     */
    public function roomGetPlayback($room_id, $page = 1, $limit = 10)
    {
        $param = [
            'action'  => 'get_replay',
            'room_id' => $room_id,
            'start'   => ($page - 1) * $limit,
            'limit'   => $limit,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxa/business/getliveinfo?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 获取直播间分享二维码
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_8-%E8%8E%B7%E5%8F%96%E7%9B%B4%E6%92%AD%E9%97%B4%E5%88%86%E4%BA%AB%E4%BA%8C%E7%BB%B4%E7%A0%81
     *
     * @param int    $roomId 直播间id
     * @param string $params 自定义参数
     * @return array|false
     */
    public function roomGetsharedcode($roomId, $params = '')
    {
        $param = [
            'roomId'       => $roomId,
            'params'       => $params,
            'access_token' => $this->access_token,
        ];
        $res   = Http::httpGet($this->api_url . '/wxaapi/broadcast/room/getsharedcode?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 删除直播间
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_5-%E5%88%A0%E9%99%A4%E7%9B%B4%E6%92%AD%E9%97%B4
     *
     * @param int $roomId 直播间id
     * @return array|false
     */
    public function roomDelete($roomId)
    {
        $param = [
            'id' => $roomId,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/deleteroom?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 获取直播间推流地址
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_7-%E8%8E%B7%E5%8F%96%E7%9B%B4%E6%92%AD%E9%97%B4%E6%8E%A8%E6%B5%81%E5%9C%B0%E5%9D%80
     *
     * @param int $roomId 直播间id
     * @return array|false
     */
    public function roomGetPushUrl($roomId)
    {
        $param = [
            'roomId'       => $roomId,
            'access_token' => $this->access_token,
        ];
        $res   = Http::httpGet($this->api_url . '/wxaapi/broadcast/room/getpushurl', $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 直播间导入商品
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_4-%E7%9B%B4%E6%92%AD%E9%97%B4%E5%AF%BC%E5%85%A5%E5%95%86%E5%93%81
     *
     * @param int   $roomId 直播间id
     * @param array $ids    商品id
     * @return array|false
     */
    public function roomGoodAdd($roomId, $ids = [])
    {
        $param = [
            'roomId' => $roomId,
            'ids'    => $ids,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/addgoods?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 上下架商品
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_21-%E4%B8%8A%E4%B8%8B%E6%9E%B6%E5%95%86%E5%93%81
     *
     * @param int $roomId  直播间id
     * @param int $goodsId 商品id
     * @param int $onSale  1上架，0下架
     * @return array|false
     */
    public function roomGoodOnsale($roomId, $goodsId, $onSale = 1)
    {
        $param = [
            'roomId'  => $roomId,
            'goodsId' => $goodsId,
            'onSale'  => $onSale,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/goods/onsale?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 推送商品
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_23-%E6%8E%A8%E9%80%81%E5%95%86%E5%93%81
     *
     * @param int $roomId  直播间id
     * @param int $goodsId 商品id
     * @return array|false
     */
    public function roomGoodPush($roomId, $goodsId)
    {
        $param = [
            'roomId'  => $roomId,
            'goodsId' => $goodsId,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/goods/push?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 商品排序
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_24-%E5%95%86%E5%93%81%E6%8E%92%E5%BA%8F
     *
     * @param int   $roomId 直播间id
     * @param array $goods  商品列表(二维数组)，格式：[{"goodsId":"123"}, {"goodsId":"234"}]
     * @return array|false
     */
    public function roomGoodSort($roomId, $goods)
    {
        // goodsId一直要是字符串型的
        foreach ($goods as &$good) {
            $good = [
                'goodsId' => strval($good['goodsId']),
            ];
            unset($good);
        }
        
        $param = [
            'roomId' => $roomId,
            'goods'  => $goods,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/goods/sort?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 添加管理直播间小助手
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_9-%E6%B7%BB%E5%8A%A0%E7%AE%A1%E7%90%86%E7%9B%B4%E6%92%AD%E9%97%B4%E5%B0%8F%E5%8A%A9%E6%89%8B
     *
     * @param int   $roomId 房间ID
     * @param array $users  用户数组，格式：[{"username":"testwechat","nickname":"testnick"}]
     * @return array|false
     */
    public function roomAddassistant($roomId, $users)
    {
        $param = [
            'roomId' => $roomId,
            'users'  => $users,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/addassistant?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 修改管理直播间小助手
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_10-%E4%BF%AE%E6%94%B9%E7%AE%A1%E7%90%86%E7%9B%B4%E6%92%AD%E9%97%B4%E5%B0%8F%E5%8A%A9%E6%89%8B
     *
     * @param int    $roomId   房间ID
     * @param string $username 用户微信号
     * @param string $nickname 用户微信昵称
     * @return array|false
     */
    public function roomModifyassistant($roomId, $username, $nickname)
    {
        $param = [
            'roomId'   => $roomId,
            'username' => $username,
            'nickname' => $nickname,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/modifyassistant?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 删除管理直播间小助手
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_11-%E5%88%A0%E9%99%A4%E7%AE%A1%E7%90%86%E7%9B%B4%E6%92%AD%E9%97%B4%E5%B0%8F%E5%8A%A9%E6%89%8B
     *
     * @param int    $roomId   房间ID
     * @param string $username 用户微信号
     * @return array|false
     */
    public function roomRemoveassistant($roomId, $username)
    {
        $param = [
            'roomId'   => $roomId,
            'username' => $username,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/removeassistant?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 查询管理直播间小助手
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_12-%E6%9F%A5%E8%AF%A2%E7%AE%A1%E7%90%86%E7%9B%B4%E6%92%AD%E9%97%B4%E5%B0%8F%E5%8A%A9%E6%89%8B
     *
     * @param int $roomId 房间ID
     * @return array|false
     */
    public function roomGetassistantlist($roomId)
    {
        $param = [
            'roomId'       => $roomId,
            'access_token' => $this->access_token,
        ];
        $res   = Http::httpGet($this->api_url . '/wxaapi/broadcast/room/getassistantlist', $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 添加主播副号
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_13-%E6%B7%BB%E5%8A%A0%E4%B8%BB%E6%92%AD%E5%89%AF%E5%8F%B7
     *
     * @param int    $roomId   房间ID
     * @param string $username 用户微信号
     * @return array|false
     */
    public function roomAddsubanchor($roomId, $username)
    {
        $param = [
            'roomId'   => $roomId,
            'username' => $username,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/addsubanchor?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 修改主播副号
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_14-%E4%BF%AE%E6%94%B9%E4%B8%BB%E6%92%AD%E5%89%AF%E5%8F%B7
     *
     * @param int    $roomId   房间ID
     * @param string $username 用户微信号
     * @return array|false
     */
    public function roomModifysubanchor($roomId, $username)
    {
        $param = [
            'roomId'   => $roomId,
            'username' => $username,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/modifysubanchor?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 删除主播副号
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_15-%E5%88%A0%E9%99%A4%E4%B8%BB%E6%92%AD%E5%89%AF%E5%8F%B7
     *
     * @param int $roomId 房间ID
     * @return array|false
     */
    public function roomDeletesubanchor($roomId)
    {
        $param = [
            'roomId' => $roomId,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/deletesubanchor?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 获取主播副号
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_16-%E8%8E%B7%E5%8F%96%E4%B8%BB%E6%92%AD%E5%89%AF%E5%8F%B7
     *
     * @param int $roomId 房间ID
     * @return array|false
     */
    public function roomGetsubanchor($roomId)
    {
        $param = [
            'roomId'       => $roomId,
            'access_token' => $this->access_token,
        ];
        $res   = Http::httpGet($this->api_url . '/wxaapi/broadcast/room/getsubanchor', $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 开启/关闭直播间官方收录
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_17-%E5%BC%80%E5%90%AF-%E5%85%B3%E9%97%AD%E7%9B%B4%E6%92%AD%E9%97%B4%E5%AE%98%E6%96%B9%E6%94%B6%E5%BD%95
     *
     * @param int $roomId        房间ID
     * @param int $isFeedsPublic 是否开启官方收录 【1: 开启，0：关闭】
     * @return array|false
     */
    public function roomUpdatefeedpublic($roomId, $isFeedsPublic)
    {
        $param = [
            'roomId'        => $roomId,
            'isFeedsPublic' => $isFeedsPublic,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/updatefeedpublic?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 开启/关闭回放功能
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_18-%E5%BC%80%E5%90%AF-%E5%85%B3%E9%97%AD%E5%9B%9E%E6%94%BE%E5%8A%9F%E8%83%BD
     *
     * @remark 直播开始后[允许]开启
     * @param int $roomId      房间ID
     * @param int $closeReplay 是否关闭回放 【0：开启，1：关闭】
     * @return array|false
     */
    public function roomUpdatereplay($roomId, $closeReplay)
    {
        $param = [
            'roomId'      => $roomId,
            'closeReplay' => $closeReplay,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/updatereplay?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 开启/关闭客服功能
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_19-%E5%BC%80%E5%90%AF-%E5%85%B3%E9%97%AD%E5%AE%A2%E6%9C%8D%E5%8A%9F%E8%83%BD
     *
     * @remark 直播开始后[允许]开启
     * @param int $roomId  房间ID
     * @param int $closeKf 是否关闭客服 【0：开启，1：关闭】
     * @return array|false
     */
    public function roomUpdatekf($roomId, $closeKf)
    {
        $param = [
            'roomId'  => $roomId,
            'closeKf' => $closeKf,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/updatekf?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 开启/关闭直播间全局禁言
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_20-%E5%BC%80%E5%90%AF-%E5%85%B3%E9%97%AD%E7%9B%B4%E6%92%AD%E9%97%B4%E5%85%A8%E5%B1%80%E7%A6%81%E8%A8%80
     *
     * @remark 直播开始后[不允许]开启
     * @param int $roomId     房间ID
     * @param int $banComment 1-禁言，0-取消禁言
     * @return array|false
     */
    public function roomUpdatecomment($roomId, $banComment)
    {
        $param = [
            'roomId'     => $roomId,
            'banComment' => $banComment,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/room/updatecomment?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 下载商品讲解视频
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/studio-api.html#_25-%E4%B8%8B%E8%BD%BD%E5%95%86%E5%93%81%E8%AE%B2%E8%A7%A3%E8%A7%86%E9%A2%91
     *
     * @remark 直播开始后[不允许]开启
     * @param int $roomId  房间ID
     * @param int $goodsId 商品ID
     * @return array|false
     */
    public function roomGetVideo($roomId, $goodsId)
    {
        $param = [
            'roomId'  => $roomId,
            'goodsId' => $goodsId,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/goods/getVideo?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
}
