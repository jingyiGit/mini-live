<?php

namespace MiniLive;

/**
 * 商品管理
 */
trait good
{
    /**
     * 商品添加并提审
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/commodity-api.html#1
     *
     * @param array $param 商品信息
     * @return array|false
     */
    public function goodAdd($param)
    {
        // 封面
        $param['coverImgUrl'] = trim($param['coverImgUrl']);
        if ($param['coverImgUrl']) {
            // 如果是远程图片，先下载到本地
            if (strpos($param['coverImgUrl'], 'http') !== false) {
                $param['coverImgUrl'] = $this->downTempImage($param['coverImgUrl']);
            }
            
            // 商品封面，高或宽不能超过300px
            $image_info = getimagesize($param['coverImgUrl']);
            if ($image_info[0] > 300 || $image_info[1] > 300) {
                $this->setError(['msg' => '商品封面，高或宽不能超过300px']);
                return false;
            }
            
            // 如果是本地图片，直接上传
            if (file_exists($param['coverImgUrl'])) {
                if ($media_info = $this->mediaUpload($param['coverImgUrl'])) {
                    $param['coverImgUrl'] = $media_info['media_id'];
                } else {
                    return false;
                }
            }
        }
        $param['url'] = ltrim($param['url'], '/');
        $res          = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/goods/add?access_token=' . $this->access_token, [
            'goodsInfo' => $param,
        ]);
        return $this->handleReturn($res);
    }
    
    /**
     * 商品撤回审核
     * 可撤回直播商品的提审申请，消耗的提审次数不返还，注意：必须是已审核通过的才可以撤回
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/commodity-api.html#2
     *
     * @param string $goodsId 商品ID(添加商品时，微信接口返回的商品ID)
     * @param string $auditId 审核单ID
     * @return array|false
     */
    public function goodResetAudit($goodsId, $auditId)
    {
        $param = [
            'goodsId' => $goodsId,
            'auditId' => $auditId,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/goods/resetaudit?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 删除商品
     * 可删除【小程序直播】商品库中的商品，删除后直播间上架的该商品也将被同步删除，不可恢复
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/commodity-api.html#4
     *
     * @param string $goodsId 商品ID(添加商品时，微信接口返回的商品ID)
     * @return array|false
     */
    public function goodDelete($goodsId)
    {
        $param = [
            'goodsId' => $goodsId,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/goods/delete?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    
    /**
     * 更新商品
     * 审核通过的商品仅允许更新价格类型与价格，审核中的商品不允许更新，未审核的商品允许更新所有字段， 只传入需要更新的字段
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/commodity-api.html#5
     *
     * @remark 在直播间的商品只能动态更改价格(商品名和封面不能更改)，好像需要重新提审
     * @param array $param 商品信息
     * @return array|false
     */
    public function goodUpdate($param)
    {
        if (!$param['goodsId']) {
            $this->setError(['msg' => '参数：商品ID(goodsId)不能为空']);
            return false;
        }
        // 封面
        $param['coverImgUrl'] = trim($param['coverImgUrl']);
        if ($param['coverImgUrl']) {
            // 如果是远程图片，先下载到本地
            if (strpos($param['coverImgUrl'], 'http') !== false) {
                $param['coverImgUrl'] = $this->downTempImage($param['coverImgUrl']);
            }
            
            // 商品封面，高或宽不能超过300px
            $image_info = getimagesize($param['coverImgUrl']);
            if ($image_info[0] > 300 || $image_info[1] > 300) {
                $this->setError(['msg' => '商品封面，高或宽不能超过300px']);
                return false;
            }
            
            // 如果是本地图片，直接上传
            if (file_exists($param['coverImgUrl'])) {
                if ($media_info = $this->mediaUpload($param['coverImgUrl'])) {
                    $param['coverImgUrl'] = $media_info['media_id'];
                } else {
                    return false;
                }
            }
        }
        $param['url'] = ltrim($param['url'], '/');
        $res          = Http::httpPostJson($this->api_url . '/wxaapi/broadcast/goods/update?access_token=' . $this->access_token, [
            'goodsInfo' => $param,
        ]);
        return $this->handleReturn($res);
    }
    
    /**
     * 获取商品状态
     * 获取商品的信息与审核状态
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/commodity-api.html#6
     *
     * @param array $goods_ids 商品ID(添加商品时，微信接口返回的商品ID)
     * @return array|false
     */
    public function goodGetStatus($goods_ids)
    {
        $param = [
            'goods_ids' => $goods_ids,
        ];
        $res   = Http::httpPostJson($this->api_url . '/wxa/business/getgoodswarehouse?access_token=' . $this->access_token, $param);
        return $this->handleReturn($res);
    }
    
    /**
     * 获取商品列表
     * https://developers.weixin.qq.com/miniprogram/dev/platform-capabilities/industry/liveplayer/commodity-api.html#7
     *
     * @param int $page   页码
     * @param int $limit  每页数量
     * @param int $status 商品状态，0：未审核，1：审核中，2：审核通过，3：审核驳回
     * @return array|false
     */
    public function goodGetList($page, $limit = 30, $status = 1)
    {
        $param = [
            'offset'       => ($page - 1) * $limit,
            'limit'        => $limit,
            'status'       => $status,
            'access_token' => $this->access_token,
        ];
        $res   = Http::httpGet($this->api_url . '/wxaapi/broadcast/goods/getapproved', $param);
        return $this->handleReturn($res);
    }
}
